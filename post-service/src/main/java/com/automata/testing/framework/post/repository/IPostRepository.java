package com.automata.testing.framework.post.repository;
/*
 * Copyright: Copyright (c) Automata akt.io 2022
 */

import org.springframework.data.repository.CrudRepository;

import com.automata.testing.framework.post.model.PostEntity;
import org.springframework.stereotype.Repository;

/**
 * Dependencies
 */

/**
 * The post repository.
 *
 * @author GELIBERT
 */
@Repository
public interface IPostRepository extends CrudRepository<PostEntity, Integer> {

    // -------------------------------------- Public methods

}
