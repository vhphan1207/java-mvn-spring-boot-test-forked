package com.automata.testing.framework.algorithm.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/*
 * Copyright: Copyright (c) Automata akt.io 2021
 */

/**
 * Dependencies
 */

/**
 * Basic Decryption Management Service Impl
 *
 * @author GELIBERT
 */
@Service
@Slf4j
public class DecrypteRotation13ServiceImpl implements IDecryptionService {

    // -------------------------------------- Inner classes

    // -------------------------------------- public static attributes

    // -------------------------------------- private static attributes

    // -------------------------------------- private attributes

    // -------------------------------------- public attributes

    // -------------------------------------- Constructor

    // -------------------------------------- Public static methods

    // -------------------------------------- Private static methods

    // -------------------------------------- Private methods

    @Override
    public String decode(String input) {
      System.out.println("Input is " + input);
      // TODO Implement this method
      if (input == null) {
        return null;
      }
      String res = "";
      for(char c : input.toCharArray()) {
        char shiftedChar = (char) (c - 13);
        char initialChar = Character.isAlphabetic(shiftedChar) ? shiftedChar : c;
        res += initialChar;
      }
      return res;
    }

    // -------------------------------------- Protected methods

    // -------------------------------------- Public methods

    // -------------------------------------- Setters and Getters

}
