package com.automata.testing.framework.algorithm.service;

public interface IPermutationAlgo {
  public String execute(String input);
}
