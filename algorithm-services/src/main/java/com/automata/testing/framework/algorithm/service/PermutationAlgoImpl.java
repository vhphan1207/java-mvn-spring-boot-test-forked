package com.automata.testing.framework.algorithm.service;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This algorithm must be able to swap 2 by 2 the elements of the string.
 *
 * @author VanHau
 */
@Service
@Slf4j
@AllArgsConstructor
public class PermutationAlgoImpl implements IPermutationAlgo {

  @Autowired
  IEncryptionService encryptionService;

  @Override
  public String execute(String input) {
    if (input == null) {
      return null;
    }
    if (input.length() == 1) {
      return encryptionService.encode(input);
    }
    String res = "";
    for (int i = 0; i < input.length() - 1; i = i + 2) {
      StringBuilder sb = new StringBuilder();
      sb.append(res);
      sb.append(input.charAt(i+1));
      sb.append(input.charAt(i));
      res = sb.toString();
    }
    if (input.length() % 2 == 1) {
      StringBuilder sb = new StringBuilder();
      sb.append(res);
      sb.append(encryptionService.encode(String.valueOf(input.charAt(input.length() - 1))));
      res = sb.toString();
    }
    return res;
  }
}
